package com.accenture.beans;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ProductBean implements Serializable{
	private Integer id;
	private String name;
	private String brand;
	private Double price;
}
