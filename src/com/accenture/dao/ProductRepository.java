package com.accenture.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.accenture.entity.ProductEntity;

@Repository
public interface ProductRepository extends JpaRepository<ProductEntity, Integer>{
	
	//1.
	public ProductEntity findById(Integer id);
	
	//2.
	public List<ProductEntity> findByNameIgnoreCase(String name);
	
	//3.
	public List<ProductEntity> findByBrand(String brand);
	
	//4.
	public List<ProductEntity> findByPriceLessThan(Double price);
	
	//5.
	public List<ProductEntity> findByPriceGreaterThan(Double price);
	
	//6.
	public List<ProductEntity> findByNameAndBrand(String name, String brand);
	
	//7.
//	@Query("select p from ProductEntity p where p.name like :name or p.brand like :name")
//	public List<ProductEntity> getAllByNameOrBrand(@Param("name")String name);
	
	//8.
	public List<ProductEntity> findByNameOrBrandLike(String name);
	
}
