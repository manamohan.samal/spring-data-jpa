package com.accenture.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@ComponentScan(basePackages = "com.accenture")
@Import(SpringDBConfig.class)
@EnableJpaRepositories("com.accenture.dao")
public class SpringMainConfig {

}
