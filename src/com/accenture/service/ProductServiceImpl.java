package com.accenture.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.accenture.beans.ProductBean;
import com.accenture.dao.ProductRepository;
import com.accenture.entity.ProductEntity;

@Service
public class ProductServiceImpl implements ProductService {

	@Autowired
	private ProductRepository productRepository;

	@Override
	public List<ProductBean> getAllByNameOrBrand(String name) {
		List<ProductEntity> entityList = productRepository.findByNameOrBrandLike("com");

		List<ProductBean> beanList = entityList.stream().map(ProductService::convertEntityToBean)
				.collect(Collectors.toList());
		return beanList;
	}

	@Override
	public Integer save(ProductBean bean) {
		ProductEntity entity = ProductService.convertBeanToEntity(bean);
		return productRepository.save(entity).getId();
	}

}
