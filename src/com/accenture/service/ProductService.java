package com.accenture.service;

import java.util.List;

import org.springframework.beans.BeanUtils;

import com.accenture.beans.ProductBean;
import com.accenture.entity.ProductEntity;


public interface ProductService {
	public List<ProductBean> getAllByNameOrBrand(String name);
	
	public Integer save(ProductBean bean);
	
	
	public static ProductBean convertEntityToBean(ProductEntity entity) {
		ProductBean bean = new ProductBean();
		BeanUtils.copyProperties(entity, bean);
		return bean;
	}
	public static ProductEntity convertBeanToEntity(ProductBean bean) {
		ProductEntity entity = new ProductEntity();
		BeanUtils.copyProperties(bean, entity);
		return entity;
	}
}
